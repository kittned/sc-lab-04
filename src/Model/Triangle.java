package Model;

public class Triangle {
	
	private String str;
	// t=1 Print 3 rows 4 stars
	// t-2 Print 4 rows 3 stars
	// t=3 Print 4 rows of length 1,2,3,4
	// t=4 Print stars in even column , dashes in odd columns
	// t=5 Print a checkerboard pattern.
	public String Type1(int n){
		String str = "";
		for(int i = 1 ; i<= n; i++){
			for(int j = 1; j<= n; j++)
				str += "*";
			str += "\n";
		}
	return str;
	}
	
	public String Type2(int n){
		String str = "";
		for(int i = 1 ; i<= n; i++){
			for(int j = 1; j<= n; j++)
				str += "*";
			str += "\n";
		}
	return str;
	}
	
	public String Type3(int n){
		String str = "";
		for(int i = 1; i <= n; i++){
			for (int j = 1; j <= i; j++)
				str += "*";
			str += "\n";
		}
	return str;
	}
	
	public String Type4(int n){
		String str = "";
		for(int i =1; i <= n ; i++){
			for(int j = 1; j<= n; j++)
				if (j%2 == 0){
					str += "*";
				}
				else {
					str +="_";
				}
		str += "\n";
		}
		return str;
	}
	
	public String Type5(int n){
		String str = "";
		for(int i = 1; i <= n; i++){
			for(int j =1; j<= n; j++){
				if((i+j)%2 == 0){
					str += "*";
				}
				else {
					str += " ";
				}
			}
		str += "\n";
		}
		return str;
	}

}
