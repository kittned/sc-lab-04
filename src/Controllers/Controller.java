package Controllers;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.JFrame;

import Model.Triangle;
import Veiw.VeiwGui;
import Controllers.Controller;


public class Controller {
	
	private VeiwGui veiw;
	private Triangle model;
	private ActionListener list;
	
	public static void main(String[] args) {
		new Controller();
	}
	
	public Controller()
	{
		model = new Triangle();
		veiw = new VeiwGui();
		veiw.pack();
		veiw.setSize(730,400);	
		veiw.setVisible(true);
		veiw.getContentPane().setLayout(new GridLayout(0,2));
		veiw.setListener(list);
	    veiw.setListener(new ActionListener() {
		        public void actionPerformed(ActionEvent event){
		        	veiw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					System.exit(0);		   	               	
		         }            
			});
	        
	        veiw.setListener2(new ActionListener() {
		        public void actionPerformed(ActionEvent event){
		        	
		        	String t1 ="Type 1: Print 3 rows 4 stars";
		        	if (t1 == veiw.type.getSelectedItem()){
		        		int input = Integer.parseInt(veiw.getInput());
		        		veiw.setResult(model.Type1(input));
		        	}
		        	
		        	String t2 ="Type 2: Print 4 rows 3 stars";
		        	if (t2 == veiw.type.getSelectedItem()){
		        		int input = Integer.parseInt(veiw.getInput());
		        		veiw.setResult(model.Type2(input));
		        	}
		        	
		        	String t3 ="Type 3: Print 4 rows of length 1-4";
		        	if (t3 == veiw.type.getSelectedItem()){
		        		int input = Integer.parseInt(veiw.getInput());
		        		veiw.setResult(model.Type3(input));
		        	}
		        	String t4 = "Type 4: Print stars in even column , dashes in odd columns";
		        	if (t4 == veiw.type.getSelectedItem()){
		        		int input = Integer.parseInt(veiw.getInput());
		        		veiw.setResult(model.Type4(input));
		        	}
		        	
		        	String t5 = "Type 5: Print a checkerboard pattern";
		        	if (t5 == veiw.type.getSelectedItem()){
		        		int input = Integer.parseInt(veiw.getInput());
		        		veiw.setResult(model.Type5(input));
		        	}
		         }      
			});
		
		
	}
	
	

}
