package Veiw;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument.Content;

import Model.Triangle;

public class VeiwGui extends JFrame {
	
	JFrame frame;
	JTextArea area;
	JLabel label1;
	JLabel label2;
	JPanel pnl1;
	JPanel pnl2;
	JPanel pnl3;
	JTextField input;
	
	JButton drawbutton;
	JButton exitbutton;
	
	String str = "";
	
	public JComboBox<String> type;
	
	public VeiwGui(){
		createFrame();
	}
	public void createFrame(){
		
		pnl1 = new JPanel();
		pnl2 = new JPanel();
		
		area = new JTextArea(30,30);
		
		input = new JTextField(15);
		
		drawbutton = new JButton("Draw");
		drawbutton.setBounds(0,0,100,50);
		
		exitbutton = new JButton("Exit Program");
		exitbutton.setBounds(0,0,100,50);
		
		type = new JComboBox<String>();
		type.setBounds(0,100,200,50);
		type.addItem("Type 1: Print 3 rows 4 stars");
		type.addItem("Type 2: Print 4 rows 3 stars");
		type.addItem("Type 3: Print 4 rows of length 1-4");
		type.addItem("Type 4: Print stars in even column , dashes in odd columns");
		type.addItem("Type 5: Print a checkerboard pattern");
		type.setSelectedIndex(0);
		
		pnl1.add(area);
		pnl2.add(type);	
		pnl2.add(input);
		pnl2.add(drawbutton);
		pnl2.add(exitbutton);
		
		add(pnl1);
		add(pnl2);
		}
	
	public void setResult(String str){
		this.str = str;
		area.setText(str);
		}
	
	public String getInput(){
		 return input.getText();
	   }
	
	public void setListener(ActionListener list){
		 exitbutton.addActionListener(list);
	   }
	
	public void setListener2(ActionListener list){
		 drawbutton.addActionListener(list);
	   }
	

}
